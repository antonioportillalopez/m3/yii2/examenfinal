<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Ejercicio10;
use Yii;


class PruebasController extends Controller
{
     public $layout = 'pruebas';
    
    public function actionUno()
    {
        return "Hola Mundo";
    }
    
    public function actionDos()
    {

    return (function (){ // devuelve el valor de la funcion anonima
            $date = new \DateTime();

            $date->modify('+2 days');
            $date->modify('+10 minute');

            return $date->format('d-m-Y H:i:s'); // Devuelve String de la fecha y hora de la funcion anómima
    })(); // ejecuta la funcion anonima

    }
    
    public function actionTres()
    {
        return $this->render('tres');
    }
    
    public function actionIndex(){
        return $this->render('index');
    }
    
        public function actionCuatro()
    {
           $model = new Ejercicio10();
        if ($model->load(Yii::$app->request->post())&& $model->calcular($model->texto)){ // si se ha dado al boton de submitir mirara la segunda condicion con lo que la ejecutará y devolvera un true para admitir que tambien es cierta la segunda condición
        
         Yii::$app->session->setFlash('contactFormSubmitted'); // envia una variable global para reutilizar la misma vista.
        
        } 
        
    return $this->render('cuatro', [ 'model' => $model, ]); // en los dos casos envia datos a la vista
        
    }
    

    
}
