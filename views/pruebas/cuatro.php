<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'ejercicio 10';
?>
<div class="site-index">
    
    
    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">  Texto: <?=$model->getTexto()?> </div>
        <div class="alert alert-success">  Número de vocales: <?=$model->getVocalestxt()?> </div>
        <div class="alert alert-success">  Longitud: <?=$model->getLongitudtxt()?> </div>
        
            
    <?php else: ?>

 
        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'texto')->textarea(['rows' => 3]) ?> <!-- ActiveForm no admite grabar en propiedades privadas (vamos que a lo mejor si pero no lo encuentro) :-) -->


                    <div class="form-group">
                        <?= Html::submitButton('Ver propiedades del texto', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
    
    
    
    
    
    
    
</div>
