<?php
use yii\bootstrap\Nav; //cargar librerias de clases de html
$this->title = 'Indice de Pruebas'; //poner el título del html que se utiliza
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Parte A</h1>
        <h2>Uno - Controlador llamado pruebas que lanza una acción Uno que muestra "Hola mundo" (sin vista).</h2>
        <h2>Dos - Controlador llamado pruebas que lanza una acción Dos que muestra "fecha+2 días + 10 minutos" utilizando método modify() de clase Datetime(){} (sin vista y devolviendo una funcion anónima funtion(){..}();</h2>
        <h2>Tres - "Hola Mundo" con su pripio archivo de vistas.</h2>
        <h2>Cuatro - Calcular las vocales y longitud de una frase.</h2>
        
    </div>
    
</div>
