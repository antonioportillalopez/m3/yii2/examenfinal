<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Ejercicio10 extends Model
{
    public $texto; /* propiedad donde se almacenará el texto del ejercicio */
    private $vocalestxt;
    private $longitudtxt;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['texto'], 'required'],
            
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'texto' => 'Introduzca un texto para averiguar sus vocales y logitud de lo escrito',
        ];
    }
    
    public function calcular($txt)
    {
            
            $txt = str_replace(['á','é','í','ó','ú','Á','É','Í','Ó','Ú'],'a',$txt);

            $this->longitudtxt=strlen($txt);
            
            $txt=strtolower($txt);
            
            $vocalesEncontradas = 0;
                       
            
            for ($indice = 0; $indice < $this->longitudtxt; $indice++) { 

                    if (
                        in_array($txt[$indice], ["a", "e", "i", "o", "u" ])
                       ) 
                                     { $vocalesEncontradas++;}
            }
            $this->vocalestxt=$vocalesEncontradas;
            return true;
    }
    
    function getTexto(){
        return $this->texto;
    }
    
    function setTexto($texto): void {
        $this->texto = $texto;
    }
    
    function getVocalestxt() {
        return $this->vocalestxt;
    }

    function getLongitudtxt() {
        return $this->longitudtxt;
    }




    
    
}
