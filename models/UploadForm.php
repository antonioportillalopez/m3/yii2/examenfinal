<?php

namespace app\models;


use yii\base\Model;




class UploadForm extends Model {
    
    public $imageFile;
    public $rutaarchivolocal;

    
    /**
     * @return array the validation rules.
     */

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg'],
        ];
    }
    
    
    public function upload()
    {
        if ($this->validate()) {
            $this->rutaarchivolocal='uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($this->rutaarchivolocal);
            return true;
        } else {
            return false;
        }
    }
}
