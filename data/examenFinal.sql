/* Creación de la Base de datos */
DROP DATABASE IF EXISTS examenfinal;

CREATE DATABASE examenfinal
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

USE `examenfinal`; /* Seleccionar base de datos a utilizar  */
/* Fin de creación de Base de datos */


-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-07-2020 a las 10:43:27
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+01:00";



--
-- Base de datos: `examenfinal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `habitantes` float DEFAULT NULL,
  `escudo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mapa` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `nombre`, `habitantes`, `escudo`, `mapa`) VALUES
(1, 'Santander', 300000, 'https://vexilologia.org/wp-content/uploads/2016/01/balbc.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(2, 'Madrid', 5805430, 'https://vexilologia.org/wp-content/uploads/2016/01/balmc.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(3, 'Bilbao', 456455000000, 'http://vexilologia.org/wp-content/uploads/2018/03/nuevoregportada.png', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(4, 'Guatemala', 54545600, 'https://vexilologia.org/wp-content/uploads/2016/01/ezgzc.png', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(5, 'Singapur', 5454550, 'https://vexilologia.org/wp-content/uploads/2016/01/evitc.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(6, 'Arrecife', 4545450, 'https://vexilologia.org/wp-content/uploads/2016/01/evigo.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(7, 'Santa Cruz', 5454, 'https://vexilologia.org/wp-content/uploads/2016/01/etgnc.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68'),
(8, 'Roma', 2343430, 'https://vexilologia.org/wp-content/uploads/2016/01/balbp.gif', 'https://goo.gl/maps/WcaPo4K36xXrWRQ68');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/* tablas para la base de datos de cursos */

-- ------------
-- tabla curso
-- ------------
USE `examenfinal`; DROP TABLE IF EXISTS curso; -- se deja esta parte por si se modifica la tabla solamente
CREATE TABLE curso(
codigocurso INT(11) AUTO_INCREMENT PRIMARY KEY,
titulo VARCHAR(255) NOT NULL, -- se pone como dato no en blanco para tener la mínima referencia del curso
descripcion VARCHAR(255),
textocorto TEXT,
textolargo TEXT,
fechacomienzo DATETIME,
fechafin DATETIME,
duracionhoras INT(11),
destacado TINYINT(1),
fotoportada VARCHAR(255),
pdf VARCHAR(255),
comenzado TINYINT(1),
finalizado TINYINT(1)
);
-- fin de tabla curso


-- ------------
-- tabla inscripciones relacionada con curso
-- ------------
USE `examenfinal`; DROP TABLE IF EXISTS inscripciones;
CREATE TABLE inscripciones(
id INT(11) AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(255),
apellidos  VARCHAR(255),
email  VARCHAR(255),
fechanacimeinto DATETIME,
estudios  VARCHAR(255),
telefono  VARCHAR(255),
desempleado TINYINT(1),
idcurso INT(11) -- campo con el valor foránea que exista en la tabla curso
);
ALTER TABLE inscripciones
ADD CONSTRAINT fk_inscripciones_idcursos_codigocurso
FOREIGN KEY (idcurso) REFERENCES curso(codigocurso);
-- fin de tabla inscripciones
